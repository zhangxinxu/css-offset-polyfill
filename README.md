# css-offset-polyfill

#### 介绍
Polyfill CSS offset-path属性中的circle()、ellipse()、inset()、polygon()函数。

#### 实现原理
借助CSS自定义属性让浏览器无法识别的语法也能够正常保留，这样就能够对这些不支持的语法进行解析与支持。

详细原理参见这里：https://www.zhangxinxu.com/wordpress/?p=9537


#### 使用示意

1. 引入JS，例如：
```html
<script src="./offset-path.js"></script>
```
2. 需要使用offset-path其他函数语法的元素新增一个自定义属性`is-offset-path`，例如：
```html
<img src="horse.png" class="horse-run" is-offset-path>
```
3. 使用CSS自定义属性设置相关的运动路径函数，例如这里使用外部的SVG元素路径：
```css
.horse-run {
 --offset-path: url(#road);
 offset-path: var(--offset-path);
 animation: motion 3s linear infinite;
}
```

页面中的SVG元素代码如下：
```html
<svg width="280" height="150" viewBox="0 0 280 150">
 <path id="road" d="M10,80 q100,120 120,20 q140,-50 160,0"/>
</svg>
```

4. 效果自动完成，此时，offset-path.js会把url(#road)转换成浏览器可以识别的path()函数，于是就有了对应的运动效果。


#### 更多案例

此demo页面包含其他`offse-path`属性函数的Polyfill效果示意：https://www.zhangxinxu.com/study/202008/offset-path-polyfill.php


#### 许可证

MIT
